import { Route, BrowserRouter, Routes } from 'react-router-dom';
import { ErrorBoundary, Loading } from '@shared/components';
import { LazyOrderbookContainer } from '@web/orderbook';
import { Suspense } from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import { extendTheme } from '@chakra-ui/react';

const theme = extendTheme({
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false,
  },
});

export const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <ErrorBoundary>
        <Suspense fallback={<Loading />}>
          <BrowserRouter>
            <Routes>
              <Route path="*" element={<LazyOrderbookContainer />} />
            </Routes>
          </BrowserRouter>
        </Suspense>
      </ErrorBoundary>
    </ChakraProvider>
  );
};
