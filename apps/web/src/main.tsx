import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';
import { store } from '@shared/store';
import { Provider } from 'react-redux';

import { App } from './app/App';

ReactDOM.render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>,
  document.getElementById('root')
);
