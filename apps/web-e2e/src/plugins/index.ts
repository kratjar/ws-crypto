/* eslint-disable */
const wp = require('@cypress/webpack-preprocessor');
const { getWebpackConfig } = require('@nrwl/cypress/plugins/preprocessor');

module.exports = (on, config) => {
  const wpConfig = getWebpackConfig(config);

  wpConfig['node'] = { fs: 'empty', child_process: 'empty', readline: 'empty' };
  wpConfig.module.rules.push({
    use: [{ loader: 'cypress-cucumber-preprocessor/loader' }],
    test: /\.feature$/,
  });

  on('file:preprocessor', wp({ webpackOptions: wpConfig }));
};
