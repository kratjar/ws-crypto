import { Server, WebSocket } from 'mock-socket';

export const mockSocketServer = (
  url: string,
  snapshotBtc: unknown,
  deltaBtc: unknown,
  snapshotEth: unknown
) => {
  const server = new Server(url);

  server.on('connection', (socket) => {
    socket.on('message', (message: string) => {
      const data = JSON.parse(message);

      if (data.event === 'unsubscribe') {
        socket.send(JSON.stringify({ event: 'unsubscribed' }));
        return;
      }

      if (data.product_ids?.includes('PI_XBTUSD')) {
        socket.send(JSON.stringify(snapshotBtc));

        setTimeout(() => {
          socket.send(JSON.stringify(deltaBtc));
        }, 500);
      }

      if (data.product_ids?.includes('PI_ETHUSD')) {
        socket.send(JSON.stringify(snapshotEth));
      }
    });

    setTimeout(() => {
      socket.close();
      server.stop();
    }, 1000);
  });

  return new WebSocket(url);
};

export const visitWithWebsocketStub = (
  url: string,
  onBeforeLoad: (win: Cypress.AUTWindow) => void = () => null
) => {
  cy.fixture('orderbook/snapshot-btc').then((snapshotBtc) => {
    cy.fixture('orderbook/snapshot-eth').then((snapshotEth) => {
      cy.fixture('orderbook/delta-btc').then((deltaBtc) => {
        cy.visit(url, {
          onBeforeLoad(win: Cypress.AUTWindow) {
            onBeforeLoad(win);

            cy.stub(win, 'WebSocket', (url) => {
              if (url.includes('ws://localhost:8080')) {
                return mockSocketServer(
                  url,
                  snapshotBtc,
                  deltaBtc,
                  snapshotEth
                );
              }
            });
          },
        });
      });
    });
  });
};
