import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';
import type { TableDefinition } from 'cypress-cucumber-preprocessor';
import { visitWithWebsocketStub } from '../../common/steps';

const ORDERBOOK_TITLE = '[data-cy="orderbook-title"]';
const ORDERBOOK_ASKS_CONTAINER = '[data-cy="orderbook-asks-container"]';
const ORDERBOOK_BIDS_CONTAINER = '[data-cy="orderbook-bids-container"]';
const ORDERBOOK_ORDER_PRICE = '[data-cy="orderbook-order-price"]';
const ORDERBOOK_ORDER_SIZE = '[data-cy="orderbook-order-size"]';
const ORDERBOOK_ORDER_TOTAL = '[data-cy="orderbook-order-total"]';
const ORDERBOOK_SPREAD = '[data-cy="orderbook-spread"]';
const ORDERBOOK_TOGGLE_BUTTON = '[data-cy="orderbook-toggle-feed"]';

const getOrderSelector = (price: string) =>
  `[data-cy="orderbook-order-${price}"]`;

Given('I go to {string} and I get Orderbook data', (url: string) =>
  visitWithWebsocketStub(url)
);

When('I toggle feed', () => {
  cy.get(ORDERBOOK_TOGGLE_BUTTON).click();
});

Then('I see Orderbook title', () =>
  cy.get(ORDERBOOK_TITLE).should('have.text', 'Orderbook')
);

Then(
  'I see Spread with value {string} and percentage {string}',
  (value: string, percentage: string) =>
    cy
      .get(ORDERBOOK_SPREAD)
      .should('include.text', value)
      .and('include.text', percentage)
);

Then('I see Orderbook asks', (dataTable: TableDefinition) => {
  dataTable.hashes().forEach(({ price, size, total }) => {
    cy.get(`${ORDERBOOK_ASKS_CONTAINER} ${getOrderSelector(price)}`).as('ask');

    cy.get('@ask')
      .find(ORDERBOOK_ORDER_PRICE)
      .should('be.visible')
      .and('have.text', price);

    cy.get('@ask')
      .find(ORDERBOOK_ORDER_SIZE)
      .should('be.visible')
      .and('have.text', size);

    cy.get('@ask')
      .find(ORDERBOOK_ORDER_TOTAL)
      .should('be.visible')
      .and('have.text', total);
  });
});

Then('I see Orderbook bids', (dataTable: TableDefinition) => {
  dataTable.hashes().forEach(({ price, size, total }) => {
    cy.get(`${ORDERBOOK_BIDS_CONTAINER} ${getOrderSelector(price)}`).as('bid');

    cy.get('@bid')
      .find(ORDERBOOK_ORDER_PRICE)
      .should('be.visible')
      .and('have.text', price);

    cy.get('@bid')
      .find(ORDERBOOK_ORDER_SIZE)
      .should('be.visible')
      .and('have.text', size);

    cy.get('@bid')
      .find(ORDERBOOK_ORDER_TOTAL)
      .should('be.visible')
      .and('have.text', total);
  });
});
