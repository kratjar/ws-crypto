Feature: Orderbook Focus Lost

  Background:
    Given I go to "/" and I loose focus

  Scenario: Websocket is unsubscribed and subscribed again
    When I see paused view
    And I gain focus and click on Reconnect button
    Then I see orderbook again

