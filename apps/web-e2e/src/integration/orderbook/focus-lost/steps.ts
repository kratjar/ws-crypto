import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';
import { visitWithWebsocketStub } from '../../common/steps';

const ORDERBOOK = '[data-cy="orderbook"]';
const ORDERBOOK_PAUSED_DESCRIPTION = '[data-cy="orderbook-paused-description"]';
const ORDERBOOK_PAUSED_RECONNECT_BUTTON =
  '[data-cy="orderbook-paused-reconnect-button"]';

Given('I go to {string} and I loose focus', (url: string) => {
  visitWithWebsocketStub(url, (win) => {
    cy.stub(win.document, 'hidden').value(true);
    cy.stub(win.document, 'visibilityState').value('hidden');
  });
});

When('I see paused view', () => {
  cy.get(ORDERBOOK_PAUSED_DESCRIPTION).should(
    'have.text',
    'You have been disconnected from the feed'
  );
});

When('I gain focus and click on Reconnect button', () => {
  cy.document().then((doc) => {
    cy.stub(doc, 'hidden').value(false);
    cy.stub(doc, 'visibilityState').value('visible');
  });

  cy.document().trigger('visibilitychange');

  cy.get(ORDERBOOK_PAUSED_RECONNECT_BUTTON).click({ force: true });
});

Then('I see orderbook again', () => {
  cy.get(ORDERBOOK).should('be.visible');
});
