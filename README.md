# Orderbook

## Performance
UI is throttled for smoother run. Websocket messages are processed, 
sorted and stored in the slice, but other Orderbook data needed for the UI are 
calculated and updated based on the device using `@react-hook/throttle`. They are then stored 
in separate slice.

The application does not use web workers, they could be used for optimizing the application more 
and also for ensuring that main thread is not blocked by processing websocket messages.

## Development server

Run `npm start` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files. App 
needs environment variables to run. Create `.env` file according to `.env.example` and fill the websocket path `NX_WEBSOCKET_PATH`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deployment

Application is deployed automatically by Vercel when commit is pushed to the `main` branch. Production deployment can be found here - https://ws-crypto.vercel.app/.

## Running unit tests

Run `npm test` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `npm run test:e2e` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Depth graph

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Code scaffolding

Run `nx g @nrwl/react:component my-component --project=web` to generate a new component.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more about nx build system used in this project.
