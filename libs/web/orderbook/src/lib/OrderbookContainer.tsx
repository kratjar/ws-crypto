import { useOrderbook } from './useOrderbook';
import React from 'react';
import { Error, Loading } from '@shared/components';
import { OrderbookPaused } from './OrderbookPaused';
import { Orderbook } from './Orderbook';

const OrderbookContainer = () => {
  const { isOrdersPresent, status, isError } = useOrderbook();

  if (isError) return <Error />;

  if (status === 'PAUSED') {
    return <OrderbookPaused />;
  }

  if (!isOrdersPresent || status === 'DISCONNECTED') {
    return <Loading />;
  }

  return <Orderbook />;
};

export default OrderbookContainer;
