import { getLinearGradient } from '../OrdersService';

describe('OrdersService', () => {
  describe('getLinearGradient', () => {
    it('returns green left gradient', () => {
      const result = getLinearGradient('left', 'green', 1000, 10000);

      expect(result).toEqual(
        'linear-gradient(to left, #123534 10%, transparent 0)'
      );
    });

    it('returns red right gradient', () => {
      const result = getLinearGradient('right', 'red', 1000, 10000);

      expect(result).toEqual(
        'linear-gradient(to right, #3d1e28 10%, transparent 0)'
      );
    });
  });
});
