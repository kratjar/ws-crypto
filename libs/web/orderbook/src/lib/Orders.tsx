import { FC } from 'react';
import { Flex, Box, FlexProps } from '@chakra-ui/react';
import { getLinearGradient } from './OrdersService';
import { Order } from '@shared/store';
import { useNumberFormatter } from '@shared/hooks';

interface Props {
  orders: Order[];
  highestDepth: number;
  color: 'green' | 'red';
  bgDirection: 'left' | 'right';
  itemsDirection?: FlexProps['flexDirection'];
  hideBaseLabels?: boolean;
}

export const Orders: FC<Props> = ({
  orders,
  highestDepth,
  color,
  bgDirection,
  itemsDirection = 'column',
  hideBaseLabels = false,
}) => {
  const sizeFormatter = useNumberFormatter({
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
  });
  const priceFormatter = useNumberFormatter({
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  return (
    <>
      <Flex
        display={{ base: hideBaseLabels ? 'none' : 'flex', md: 'flex' }}
        direction={{
          base: 'row',
          md: bgDirection === 'right' ? 'row' : 'row-reverse',
        }}
        color="gray.400"
        textAlign="right"
        borderTopWidth={1}
        borderBottomWidth={0.2}
        mt={2}
        textTransform="uppercase"
      >
        <Box flex={1} mx={2} data-cy="orderbook-orders-heading-price">
          Price
        </Box>
        <Box flex={1} mx={2} data-cy="orderbook-orders-heading-size">
          Size
        </Box>
        <Box flex={1} mx={2} data-cy="orderbook-orders-heading-total">
          Total
        </Box>
      </Flex>

      <Flex direction={itemsDirection} textAlign="right">
        {orders.map(({ price, size, total = 0 }) => (
          <Flex
            key={price}
            direction={{
              base: 'row',
              md: bgDirection === 'right' ? 'row' : 'row-reverse',
            }}
            background={{
              md: getLinearGradient(bgDirection, color, total, highestDepth),
              base: getLinearGradient('right', color, total, highestDepth),
            }}
            textTransform="uppercase"
            data-cy={`orderbook-order-${priceFormatter(price)}`}
          >
            <Box flex={1} color={color} mx={2} data-cy="orderbook-order-price">
              {priceFormatter(price)}
            </Box>
            <Box flex={1} mx={2} data-cy="orderbook-order-size">
              {sizeFormatter(size)}
            </Box>
            <Box flex={1} mx={2} data-cy="orderbook-order-total">
              {sizeFormatter(total)}
            </Box>
          </Flex>
        ))}
      </Flex>
    </>
  );
};
