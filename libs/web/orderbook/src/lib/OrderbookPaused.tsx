import { Box, Button, Container, Text } from '@chakra-ui/react';
import React, { FC } from 'react';
import { useConnectMutation } from '@shared/store';

export const OrderbookPaused: FC = () => {
  const [connect] = useConnectMutation();

  return (
    <Container
      display="flex"
      justifyContent="center"
      alignItems="center"
      flexDir="column"
      height="100vh"
    >
      <Text data-cy="orderbook-paused-description">
        You have been disconnected from the feed
      </Text>
      <Box mt={2} data-cy="orderbook-paused-reconnect-button">
        <Button onClick={() => connect()}>Reconnect</Button>
      </Box>
    </Container>
  );
};
