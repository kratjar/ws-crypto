import {
  updateOrderbookSlice,
  selectOrderbookSlice,
  useOrderbookSocket,
  useOrderbookSocketPause,
} from '@shared/store';
import { useSelector } from 'react-redux';
import { useThrottledDispatch } from '@shared/store';
import { useEffect } from 'react';
import { usePageVisibility } from 'react-page-visibility';

export const useOrderbook = () => {
  const { bids, asks } = useSelector(selectOrderbookSlice);
  const isPageVisible = usePageVisibility();
  const { data: orders } = useOrderbookSocket();
  const status = orders?.status;
  const throttledDispatch = useThrottledDispatch(20);
  const [pause] = useOrderbookSocketPause();

  useEffect(() => {
    if (orders) throttledDispatch(updateOrderbookSlice(orders));
  }, [orders, throttledDispatch]);

  useEffect(() => {
    if (!isPageVisible && status !== 'PAUSED') pause();
  }, [pause, status, isPageVisible]);

  return {
    status,
    isError: orders?.isError,
    isOrdersPresent: bids.length > 0 && asks.length > 0,
  };
};
