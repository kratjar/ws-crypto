import { lazy } from 'react';

export const LazyOrderbookContainer = lazy(
  () => import('./OrderbookContainer')
);
