const getColorCode = (color: 'green' | 'red') => {
  if (color === 'green') return '#123534';

  return '#3d1e28';
};

export const getLinearGradient = (
  direction: 'left' | 'right',
  color: 'green' | 'red',
  total: number,
  highestDepth: number
) =>
  `linear-gradient(to ${direction}, ${getColorCode(color)} ${
    (total / highestDepth) * 100
  }%, transparent 0)`;
