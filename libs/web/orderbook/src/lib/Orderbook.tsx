import React, { FC, memo } from 'react';
import { selectOrderbookSlice, useToggleFeedMutation } from '@shared/store';
import { Box, Button, SimpleGrid, Stack } from '@chakra-ui/react';
import { Orders } from './Orders';
import { useSelector } from 'react-redux';
import { useNumberFormatter } from '@shared/hooks';

export const Orderbook: FC = memo(() => {
  const [toggleFeed] = useToggleFeedMutation();
  const { spread, spreadPercentage, asks, bids, highestDepth } =
    useSelector(selectOrderbookSlice);

  const spreadFormatter = useNumberFormatter({
    minimumFractionDigits: 1,
    maximumFractionDigits: 1,
  });
  const spreadPercentageFormatter = useNumberFormatter({
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  return (
    <Box m={4} data-cy="orderbook">
      <SimpleGrid columns={{ base: 1, md: 2 }}>
        <Box
          gridRow={{ base: 1 }}
          gridColumn={{ base: '1/2', md: 1 }}
          data-cy="orderbook-title"
        >
          Orderbook
        </Box>
        <Box
          gridRow={{ base: 3, md: 1 }}
          gridColumn={{ base: 1, md: '1/3' }}
          color="gray.400"
          textAlign="center"
          data-cy="orderbook-spread"
        >
          Spread: {spreadFormatter(spread)} (
          {spreadPercentageFormatter(spreadPercentage)}%)
        </Box>
        <Box gridRow={{ base: 4, md: 2 }} data-cy="orderbook-bids-container">
          <Orders
            orders={bids}
            color="green"
            highestDepth={highestDepth}
            bgDirection="left"
            hideBaseLabels
          />
        </Box>
        <Box gridRow={{ base: 2 }} data-cy="orderbook-asks-container">
          <Orders
            orders={asks}
            color="red"
            highestDepth={highestDepth}
            bgDirection="right"
            itemsDirection={{ base: 'column-reverse', md: 'column' }}
          />
        </Box>
      </SimpleGrid>
      <Stack mt={2} dir="row" align="center">
        <Button onClick={() => toggleFeed()} data-cy="orderbook-toggle-feed">
          Toggle feed
        </Button>
      </Stack>
    </Box>
  );
});
