import { createEntityAdapter } from '@reduxjs/toolkit';
import { Recipe } from '@reduxjs/toolkit/dist/query/core/buildThunks';
import { Orders, Order } from './types';
import {
  isMessageWithData,
  isUnsubscribedMessage,
  transformOrders,
  updateOrdersAdapter,
} from './SocketService';

const bidsAdapter = createEntityAdapter<Order>({
  selectId: (order) => order.price.toString(),
  sortComparer: (a, b) => Math.round(b.price * 100) - Math.round(a.price * 100),
});

const asksAdapter = createEntityAdapter<Order>({
  selectId: (order) => order.price.toString(),
  sortComparer: (a, b) => {
    return Math.round(a.price * 100) - Math.round(b.price * 100);
  },
});

export const getInitialSocketData = () => ({
  bids: bidsAdapter.getInitialState(),
  asks: asksAdapter.getInitialState(),
  status: 'DISCONNECTED',
  isError: false,
  productId: 'PI_XBTUSD',
});

export const handleSocketMessage = (
  event: MessageEvent,
  updateCachedData: (recipe: Recipe<Orders>) => void
) => {
  const data = JSON.parse(event.data);

  if (isUnsubscribedMessage(data)) {
    updateCachedData((draft) => {
      draft.bids = bidsAdapter.getInitialState();
      draft.asks = asksAdapter.getInitialState();
    });
  }

  if (!isMessageWithData(data)) return;

  updateCachedData((draft) => {
    updateOrdersAdapter(bidsAdapter, draft.bids, transformOrders(data.bids));
    updateOrdersAdapter(asksAdapter, draft.asks, transformOrders(data.asks));
  });
};
