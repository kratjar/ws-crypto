import { calculateOrderbookData } from '../SliceService';

const TEST_ASKS = [
  { price: 39213.5, size: 10000.0 },
  { price: 39214.0, size: 6569.0 },
];

const TEST_BIDS = [
  { price: 39196.5, size: 4400.0 },
  { price: 39181.0, size: 15000.0 },
];

describe('SliceService', () => {
  describe('calculateOrderbookData', () => {
    it('calculates spread', () => {
      const { spread } = calculateOrderbookData(TEST_ASKS, TEST_BIDS);

      expect(spread).toEqual(17);
    });

    it('calculates spread percentage', () => {
      const { spreadPercentage } = calculateOrderbookData(TEST_ASKS, TEST_BIDS);

      expect(spreadPercentage).toEqual(0.04);
    });

    it('calculates highest depth', () => {
      const { highestDepth } = calculateOrderbookData(TEST_ASKS, TEST_BIDS);

      expect(highestDepth).toEqual(19400);
    });

    it('calculates totals', () => {
      const { asks, bids } = calculateOrderbookData(TEST_ASKS, TEST_BIDS);

      expect(asks).toEqual([
        { price: 39213.5, size: 10000, total: 10000 },
        { price: 39214, size: 6569, total: 16569 },
      ]);
      expect(bids).toEqual([
        { price: 39196.5, size: 4400, total: 4400 },
        { price: 39181, size: 15000, total: 19400 },
      ]);
    });

    it('limits asks and bids', () => {
      const { asks, bids } = calculateOrderbookData(
        Array(20).fill({ price: 1, size: 1 }),
        Array(20).fill({ price: 1, size: 1 })
      );

      expect(asks.length).toEqual(16);
      expect(bids.length).toEqual(16);
    });
  });
});
