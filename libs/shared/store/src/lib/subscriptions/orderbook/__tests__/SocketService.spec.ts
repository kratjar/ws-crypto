import {
  getToggledProductId,
  transformOrders,
  prepareOrders,
  isMessageWithData,
  isUnsubscribedMessage,
} from '../SocketService';

describe('SocketService', () => {
  describe('isMessageWithData', () => {
    it('returns true when message has data', () => {
      const result = isMessageWithData({ feed: 'book_ui_1_snapshot' });

      expect(result).toEqual(true);
    });

    it('returns false when message does not have data', () => {
      const result = isMessageWithData({ event: 'something' });

      expect(result).toEqual(false);
    });
  });

  describe('isUnsubscribedMessage', () => {
    it('returns true when message type is unsubscribed', () => {
      const result = isUnsubscribedMessage({ event: 'unsubscribed' });

      expect(result).toEqual(true);
    });

    it('returns false when message type is not unsubscribed', () => {
      const result = isUnsubscribedMessage({ event: 'something' });

      expect(result).toEqual(false);
    });
  });

  describe('getToggledProductId', () => {
    it('returns eth product when btc is provided', () => {
      const state: any = { productId: 'PI_XBTUSD' };

      const result = getToggledProductId(state);

      expect(result).toEqual('PI_ETHUSD');
    });

    it('returns btc product when eth is provided', () => {
      const state: any = { productId: 'PI_ETHUSD' };

      const result = getToggledProductId(state);

      expect(result).toEqual('PI_XBTUSD');
    });
  });

  describe('transformOrders', () => {
    it('transform orders from array to object', () => {
      const orders: any = [
        [39213.5, 10000.0],
        [39214.0, 6569.0],
      ];

      const result = transformOrders(orders);

      expect(result).toEqual([
        { price: 39213.5, size: 10000 },
        { price: 39214, size: 6569 },
      ]);
    });
  });

  describe('prepareOrders', () => {
    const orders: any = [
      { price: 39212.0, size: 2000 },
      { price: 39213.5, size: 10000.0 },
      { price: 39214.5, size: 0 },
    ];

    it('returns order prices which should be removed', () => {
      const { ordersToRemove } = prepareOrders(orders);

      expect(ordersToRemove).toEqual([39214.5]);
    });

    it('returns orders which should be upsert', () => {
      const { ordersToUpsert } = prepareOrders(orders);

      expect(ordersToUpsert).toEqual([
        { price: 39212.0, size: 2000 },
        { price: 39213.5, size: 10000.0 },
      ]);
    });
  });
});
