import { EntityAdapter, EntityState } from '@reduxjs/toolkit';
import { Event, Order, Orders, ProductId, ResponseOrder } from './types';

interface PrepareOrdersAccumulator {
  ordersToUpsert: Order[];
  ordersToRemove: number[];
}

const MAX_ORDERS = 50;
const FEED = 'book_ui_1';
export const WEBSOCKET_PATH = process.env.NX_WEBSOCKET_PATH ?? '';

export const isMessageWithData = (data: any) => !('event' in data);

export const isUnsubscribedMessage = (data: any) =>
  data?.event === 'unsubscribed';

const prepareOrdersReducer = (acc: PrepareOrdersAccumulator, item: Order) => {
  const isOrderToRemove = item.size === 0;

  if (isOrderToRemove) acc.ordersToRemove.push(item.price);
  if (!isOrderToRemove) acc.ordersToUpsert.push(item);

  return acc;
};

export const prepareOrders = (incomingOrders: Order[]) =>
  incomingOrders.reduce(prepareOrdersReducer, {
    ordersToUpsert: [] as Order[],
    ordersToRemove: [] as number[],
  });

export const updateOrdersAdapter = (
  adapter: EntityAdapter<Order>,
  state: EntityState<Order>,
  incomingOrders: Order[]
) => {
  const { ordersToRemove, ordersToUpsert } = prepareOrders(incomingOrders);

  adapter.removeMany(state, ordersToRemove);
  adapter.upsertMany(state, ordersToUpsert);
  // Optimization: Keep just certain amount of orders - risk of data lost
  adapter.removeMany(state, state.ids.slice(MAX_ORDERS));
};

// Order transformation from array of numbers to objects
export const transformOrders = (orders: ResponseOrder[] = []) =>
  orders.map(([price, size]): Order => ({ price, size }));

export const sendMessage = (
  websocket: WebSocket,
  event: Event,
  productId: ProductId
) =>
  websocket.send(
    JSON.stringify({
      event,
      feed: FEED,
      product_ids: [productId],
    })
  );

export const getToggledProductId = (state: Orders) =>
  state.productId === 'PI_XBTUSD' ? 'PI_ETHUSD' : 'PI_XBTUSD';
