import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../../Store';
import { Order, Orders } from './types';
import {
  calculateOrderbookData,
  getSortedIncomingOrders,
  isOrderList,
} from './SliceService';

interface OrderbookSlice {
  bids: Order[];
  asks: Order[];
  spread: number;
  spreadPercentage: number;
  highestDepth: number;
}

const initialState: OrderbookSlice = {
  bids: [],
  asks: [],
  spread: 0,
  spreadPercentage: 0,
  highestDepth: 0,
};

export const orderbookSlice = createSlice({
  name: 'orderbookSlice',
  initialState,
  reducers: {
    update: (state, { payload }: PayloadAction<Orders>) => {
      const incomingAsks = getSortedIncomingOrders(payload.asks);
      const incomingBids = getSortedIncomingOrders(payload.bids);

      if (!isOrderList(incomingAsks) || !isOrderList(incomingBids)) return;

      const { spread, spreadPercentage, highestDepth, bids, asks } =
        calculateOrderbookData(incomingAsks, incomingBids);

      state.spread = spread;
      state.spreadPercentage = spreadPercentage;
      state.highestDepth = highestDepth;
      state.bids = bids;
      state.asks = asks;
    },
  },
});

export const { update } = orderbookSlice.actions;

export const selectOrderbookSlice = (state: RootState) => state.orderbookSlice;

export const { reducer } = orderbookSlice;
