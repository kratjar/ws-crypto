import { Order } from './types';
import { EntityState } from '@reduxjs/toolkit';

export const ORDER_LIMIT = 16;

const addOrdersTotals = (orders: Order[]) =>
  orders.reduce((orders: Order[], { price, size }, index) => {
    const { total: previousTotal = 0 } = orders[index - 1] || [];
    const total = previousTotal + size;
    const order: Order = { price, size, total };

    return [...orders, order];
  }, []);

const limitOrders = (limit: number, orders: Order[]) => orders.slice(0, limit);

export const isOrderList = (orders: (Order | void)[]): orders is Order[] =>
  orders.length > 0;

export const calculateOrderbookData = (asks: Order[], bids: Order[]) => {
  const limitedAsks = addOrdersTotals(limitOrders(ORDER_LIMIT, asks));
  const limitedBids = addOrdersTotals(limitOrders(ORDER_LIMIT, bids));

  const { price: minAskPrice } = limitedAsks[0];
  const { price: maxBidPrice } = limitedBids[0];
  const { total: maxAskTotal = 0 } = limitedAsks[limitedAsks.length - 1];
  const { total: minBidTotal = 0 } = limitedBids[limitedBids.length - 1];

  const spread = minAskPrice - maxBidPrice;
  const spreadPercentage = Math.round((spread / minAskPrice) * 10000) / 100;
  const highestDepth = Math.max(maxAskTotal, minBidTotal);

  return {
    spread,
    spreadPercentage,
    highestDepth,
    asks: limitedAsks,
    bids: limitedBids,
  };
};

export const getSortedIncomingOrders = (orders: EntityState<Order>) =>
  orders.ids.map((item) => orders.entities[item]);
