import { EntityState } from '@reduxjs/toolkit';

export type ProductId = 'PI_XBTUSD' | 'PI_ETHUSD';
export type Event = 'subscribe' | 'unsubscribe';
export type OrdersStatus = 'CONNECTED' | 'DISCONNECTED' | 'PAUSED';
export type ResponseOrder = [price: number, size: number, total?: number];
export type Order = { price: number; size: number; total?: number };

export interface Orders {
  bids: EntityState<Order>;
  asks: EntityState<Order>;
  status: OrdersStatus;
  isError?: boolean;
  productId: ProductId;
}
