import { createApi } from '@reduxjs/toolkit/query/react';
import {
  getToggledProductId,
  sendMessage,
  WEBSOCKET_PATH,
} from './SocketService';
import { Orders } from './types';
import {
  getInitialSocketData,
  handleSocketMessage,
} from './SocketAdapterService';

// From Docs: https://support.cryptofacilities.com/ - "In order to keep the websocket connection alive, you will need to make a ping request at least every 60 seconds."
const PING_TIME = 40000;

let websocket: WebSocket;
let interval: NodeJS.Timeout;

const pingWebsocketInInterval = () => {
  interval = setInterval(() => websocket.send('ping'), PING_TIME);
};

const clearWebsocketPing = () => clearInterval(interval);

export const orderbookSocket = createApi({
  reducerPath: 'orderbookSocket',
  baseQuery: () => ({
    data: getInitialSocketData(),
  }),
  endpoints: (build) => ({
    toggleFeed: build.mutation<void, void>({
      query: () => null,
      onQueryStarted: (_, { dispatch }) => {
        dispatch(
          orderbookSocket.util.updateQueryData(
            'getOrderbook',
            undefined,
            (state: Orders) => {
              const toggledProductId = getToggledProductId(state);

              sendMessage(websocket, 'unsubscribe', state.productId);
              sendMessage(websocket, 'subscribe', toggledProductId);

              state.productId = toggledProductId;
            }
          )
        );
      },
    }),
    pause: build.mutation<void, void>({
      query: () => null,
      onQueryStarted: (_, { dispatch }) => {
        dispatch(
          orderbookSocket.util.updateQueryData(
            'getOrderbook',
            undefined,
            (state: Orders) => {
              state.status = 'PAUSED';
              sendMessage(websocket, 'unsubscribe', state.productId);
              pingWebsocketInInterval();
            }
          )
        );
      },
    }),
    connect: build.mutation<void, void>({
      query: () => null,
      onQueryStarted: (_, { dispatch }) => {
        dispatch(
          orderbookSocket.util.updateQueryData(
            'getOrderbook',
            undefined,
            (state: Orders) => {
              state.status = 'CONNECTED';
              sendMessage(websocket, 'subscribe', state.productId);
              clearWebsocketPing();
            }
          )
        );
      },
    }),
    getOrderbook: build.query<Orders, void>({
      query: () => '/',
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        try {
          websocket = new WebSocket(WEBSOCKET_PATH);

          websocket.onopen = () => {
            updateCachedData((state) => {
              state.status = 'CONNECTED';
              sendMessage(websocket, 'subscribe', state.productId);
            });
          };

          websocket.onmessage = (event) =>
            handleSocketMessage(event, updateCachedData);

          await cacheDataLoaded;
        } catch {
          await cacheDataLoaded;

          updateCachedData((draft) => {
            draft.isError = true;
          });
        }

        await cacheEntryRemoved;
        websocket.close();
      },
    }),
  }),
});

export const {
  useGetOrderbookQuery: useOrderbookSocket,
  useToggleFeedMutation,
  useConnectMutation,
  usePauseMutation,
  reducer,
  middleware,
} = orderbookSocket;
