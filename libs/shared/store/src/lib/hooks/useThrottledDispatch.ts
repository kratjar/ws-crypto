import { useDispatch } from 'react-redux';
import { useThrottleCallback } from '@react-hook/throttle';

export const useThrottledDispatch = (fps?: number) => {
  const dispatch = useDispatch();

  return useThrottleCallback(dispatch, fps);
};
