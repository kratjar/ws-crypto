import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';

import {
  reducer as getOrderbookReducer,
  middleware as getOrderbookMiddleware,
} from './subscriptions/orderbook/useOrderbookSocket';
import { reducer as orderbookSliceReducer } from './subscriptions/orderbook/useOrderbookSlice';

export const store = configureStore({
  reducer: {
    orderbookSocket: getOrderbookReducer,
    orderbookSlice: orderbookSliceReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat(
      getOrderbookMiddleware
    ),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
