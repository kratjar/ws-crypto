import { FC } from 'react';
import { render, screen } from '@testing-library/react';
import { ErrorBoundary } from '../ErrorBoundary';

interface Props {
  shouldThrow?: boolean;
}

const ERROR_BOUNDARY_TITLE = 'error-boundary-title';
const ERROR_BOUNDARY_DESCRIPTION = 'error-boundary-description';
const TEST_NO_ERROR = 'no-error';

const Bomb: FC<Props> = ({ shouldThrow }) => {
  if (shouldThrow) {
    throw new Error('💣');
  } else {
    return <span data-cy={TEST_NO_ERROR}>There is no error</span>;
  }
};

describe('<ErrorBoundary />', () => {
  it('renders children', () => {
    render(
      <ErrorBoundary>
        <Bomb />
      </ErrorBoundary>
    );

    expect(screen.getByTestId(TEST_NO_ERROR)).toHaveTextContent(
      'There is no error'
    );
  });

  it('renders error', () => {
    render(
      <ErrorBoundary>
        <Bomb shouldThrow />
      </ErrorBoundary>
    );

    expect(screen.getByTestId(ERROR_BOUNDARY_TITLE)).toHaveTextContent(
      'Something went wrong.'
    );
    expect(screen.getByTestId(ERROR_BOUNDARY_DESCRIPTION)).toHaveTextContent(
      'Try again later.'
    );
  });
});
