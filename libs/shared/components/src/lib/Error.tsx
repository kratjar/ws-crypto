import { Container, Heading, Text } from '@chakra-ui/react';
import React from 'react';

export const Error = () => (
  <Container
    display="flex"
    justifyContent="center"
    alignItems="center"
    flexDir="column"
    height="100vh"
  >
    <Heading data-cy="error-boundary-title">Something went wrong.</Heading>
    <Text data-cy="error-boundary-description">Try again later.</Text>
  </Container>
);
