import { Container, Spinner } from '@chakra-ui/react';
import React from 'react';

export const Loading = () => (
  <Container
    display="flex"
    justifyContent="center"
    alignItems="center"
    flexDir="column"
    height="100vh"
  >
    <Spinner />
  </Container>
);
