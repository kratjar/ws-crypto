import { renderHook } from '@testing-library/react-hooks';

import { useNumberFormatter } from '../useNumberFormatter';

describe('useNumberFormatter', () => {
  it('returns formatter', () => {
    const {
      result: { current: formatter },
    } = renderHook(() => useNumberFormatter());

    expect(formatter(5)).toEqual('5');
    expect(formatter(20000)).toEqual('20,000');
    expect(formatter(0.5)).toEqual('0.5');
  });

  it('returns formatter with options specified', () => {
    const {
      result: { current: formatter },
    } = renderHook(() =>
      useNumberFormatter({ maximumFractionDigits: 2, minimumFractionDigits: 2 })
    );

    expect(formatter(5)).toEqual('5.00');
    expect(formatter(20000)).toEqual('20,000.00');
    expect(formatter(0.5)).toEqual('0.50');
  });
});
