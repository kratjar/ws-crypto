const DEFAULT_LANGUAGE = 'en';

export const useNumberFormatter = (options?: Intl.NumberFormatOptions) =>
  Intl.NumberFormat(DEFAULT_LANGUAGE, options).format;
